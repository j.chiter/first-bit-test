<?php

namespace Database\Factories;

use App\Models\Product;
use Faker\Core\Number;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => implode(' ', fake('en_US')->unique()->words()),
            'code' => fake()->postcode(),
            'color' => fake()->colorName(),
            'price' => (new Number())->randomNumber(),
            'size' => (new Number())->randomNumber(),
            'description' => fake()->text()
        ];
    }
}
