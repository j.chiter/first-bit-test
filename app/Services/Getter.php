<?php

namespace App\Services;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Services\Getter
 *
 * @property Builder $query
 */
abstract class Getter
{
    protected Builder $query;
    protected bool $isSearch = false;

    abstract public function __construct();

    final public function getQuery(): Builder
    {
        return $this->query;
    }

    public function search($search): Getter
    {
        if (!$fields = json_decode($search, true)) {
            return $this;
        }

        $this->buildQuery($this->query, $fields, true);

        return $this;
    }

    public function filter($search): static
    {
        if (!$fields = json_decode($search, true)) {
            return $this;
        }

        $this->query->where(function (Builder $query) use ($fields) {
            $query->where(function (Builder $queryEmbedded) use ($fields) {
                $this->buildQuery($queryEmbedded, $fields);
            });
            $this->updateFilterQuery($query);
        });

        return $this;
    }

    public function getInstance($instance)
    {
        $query = get_class($instance)::query();
        $getQuery = $this->query->getQuery();

        $query->select($getQuery->columns);

        foreach ($getQuery->joins as $join) {
            foreach ($join->wheres as $relation) {
                $query->leftJoin($join->table, $relation['first'], $relation['operator'], $relation['second']);
            }
        }

        if ($getQuery->groups) {
            $query->groupBy($getQuery->groups);
        }

        foreach ($getQuery->wheres as $where) {
            if ($where['type'] == 'Null' && $where['boolean'] == 'and') {
                $query->whereNull($where['column']);
            }
        }

        return $query->find($instance->id);
    }

    private function buildQuery(Builder &$query, $fields, $isSearch = false): void
    {
        foreach ($fields as $key => $value) {
            if ($value === "" || $value === null || $value === []) {
                continue;
            }

            if (is_array($value)) {
                if (empty(array_diff($value, array('', null))))
                    continue;
            }

            if (!$this->customSearch($query, $key, $value)) {
                $query->where($key, $value);
            }

            if ($isSearch) {
                $this->isSearch = true;
            }
        }

    }

    protected function updateFilterQuery(Builder &$query)
    {
    }

    protected function customSearch(Builder &$query, $key, $value)
    {
        return false;
    }

    final public function orderBy($column, $direction = 'asc'): Getter
    {
        $this->query->orderBy($column, $direction);

        return $this;
    }

    final public function paginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null): LengthAwarePaginator
    {
        return $this->query->paginate($perPage, $columns, $pageName, $page);
    }

    final public function get($columns = ['*'])
    {
        return $this->query->get($columns);
    }
}
