<?php

namespace App\Services\Product;

use App\Models\Product;
use App\Services\Getter;
use Illuminate\Database\Eloquent\Builder;

class ProductGetter extends Getter
{
    public function __construct()
    {
        $this->query = Product::query();
    }

    protected function customSearch(Builder &$query, $key, $value): bool
    {
        $isApply = true;
        $columnsLike = [
            'name',
            'description'
        ];

        $columnsFull = [
            'code',
            'color',
            'size'
        ];

        if (in_array($key, $columnsLike)) {
            $query->where($key, 'ilike', '%' . $value . '%');
        }  else if (in_array($key, $columnsFull)) {
            $query->where($key, '=', $value);
        } else if ($key == 'price1') {
            $between = explode('-', $value);
            if (is_array($between) && count($between) == 2) {
                $query->whereBetween($key, $between);
            } else {
                $query->where($key, '=', $value);
            }
        } else {
            $isApply = false;
        }

        return $isApply;
    }
}
