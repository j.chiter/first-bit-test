import defaultMixin from "./default";
export default {
    mixins: [defaultMixin],
    data: () => ({
        defaultSort: {
            prop: 'id',
            order: 'descending'
        },
        defaultRequest: true,
        search: {}
    }),
    computed: {
        appData() {
            return this.$store.getters[this.storeName + '/appData'];
        }
    },
    methods: {
        index(page, sort) {
            const params = {
                page,
                search: JSON.stringify(this.search)
            };

            if (sort) {
                params.orderBy = sort.prop ? sort.prop : this.defaultSort.prop;
                params.orderDir = sort.order === 'descending' ? 'desc' : 'asc';
            }

            this.$store.dispatch(this.storeName + '/index', params)
                .catch((error) => {
                    console.error(error);
                });
        },
        clearSearch() {
            this.search = this.defaultSearch();
            this.index(1, this.defaultSort);
        },
        defaultSearch() {
            return {};
        },
    },
    mounted() {
        this.defaultRequest ? this.index(1, this.defaultSort) : false;
    }
};
