import camelCase from 'camelcase';
export default {
    data: () => ({
        storeName: null,
        routeName: null,
        apiUrl: null
    }),
    mounted() {
        let routes = this.$route.name.split('.');
        this.routeName = routes.length > 2 ? routes[routes.length - 2] : routes[0];

        this.storeName = camelCase(this.routeName);
        this.apiUrl = '/api/' + this.routeName;
        this.$store.commit(this.storeName + '/setPath', this.apiUrl);
    }
};
