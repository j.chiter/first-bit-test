/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('../bootstrap.js');

import Vue from 'vue';
import Jquery from 'jquery';

import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/ru-RU';

import 'normalize.css/normalize.css';
import '../sass/element-variables.scss';

Vue.use(ElementUI, {locale});
window.$ = window.jquery = Jquery;
window.Vue = Vue;

import App from './components/App.vue';
import {router} from './routes';
import {store} from './store';

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
