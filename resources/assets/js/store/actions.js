import api from '../api/api';

export default {
	index({state, commit}, payload = {}) {
		let page = payload.page;

		if (!page) {
            page = state.appData.current_page ? state.appData.current_page : 1;
        }

		if (payload.orderBy) {
            commit('setOrderBy', payload.orderBy);
        }

		if (payload.orderDir) {
            commit('setOrderDir', payload.orderDir);
        }

		if (payload.search) {
            commit('setSearch', payload.search);
        }

		return new Promise((resolve, reject) => {
			api.get(state.path, {
				page,
				"order-by": state.orderBy,
				"order-dir": state.orderDir,
				search: state.search
			})
				.then(function (response) {
					commit('setAppData', response.data);
					resolve(response);
				})
				.catch(function (error) {
					reject(error);
				});
		});
	}
}
