export default {
    setPath(state, value) {
        state.path = value;
    },
    setAppData(state, value) {
        state.appData = value;
    },
    setOrderBy(state, value) {
        state.orderBy = value;
    },
    setOrderDir(state, value) {
        state.orderDir = value;
    },
    setSearch(state, value) {
        state.search = value;
    }
}
