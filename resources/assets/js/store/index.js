import Vuex from 'vuex';
import Vue from 'vue';
import defaultModule from './modules/default';

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        product: defaultModule
    }
});
