export default {
    state(module, paramName = 'id') {
        return {
            path: '/api/' + module,
            paramName,
            appData: {},
            orderBy: '',
            orderDir: 'desc',
            search: {}
        };
    }
}
