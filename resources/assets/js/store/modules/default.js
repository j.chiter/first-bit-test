import actions from '../actions';
import mutations from "../mutations";
import getters from "../getters";

export default {
	namespaced: true,
	state(module, paramName = 'id') {
		return {
			path: `/api/${module}`,
			paramName,
			appData: {},
			orderBy: '',
			orderDir: 'desc',
			search: {}
		};
	},
	getters: {
		...getters
	},
	mutations: {
		...mutations
	},
	actions: {
		...actions
	},
};
