export default [
	{
		path: '/product',
		component: () => import('../../pages/product/list.vue'),
		name: 'product',
		meta: {
			title: 'Продукты'
		}
	}
]
