import VueRouter from 'vue-router';
import Vue from 'vue';
import {store} from '../store';

import product from './modules/product';

Vue.use(VueRouter);

let router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '',
            component: () => import('../views/Layout.vue'),
            children: [
                ...product
            ]
        }
    ],
});

router.beforeEach(async (to, from, next) => {
    window.document.title = to.meta && to.meta.title ? to.meta.title : 'Главная';
    next();
});

export {router};
