const mix = require('laravel-mix');
const path = require('path');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

// Admin
mix.js('resources/assets/js/app.js', 'public/assets/js')
    .webpackConfig({
        output: {
            publicPath: '/',
            filename: '[name].js',
            chunkFilename: 'assets/js/[name].[chunkhash].chunk.js'
        },
        resolve: {
            alias: {
                '@lang': path.resolve('./resources/lang'),
            },
        },
        module: {
            rules: [
                {
                    test: /resources[\\\/]lang.+\.(php)$/,
                    loader: 'php-array-loader',
                },
            ],
        },
    })
    .sourceMaps()
    .version()
    .vue();

mix.browserSync(process.env.APP_URL);

if (mix.inProduction()) {
    mix.version();
}
